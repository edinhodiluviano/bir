FROM python:3.8.7-slim-buster

RUN mkdir -p /app/tests /app/bir /app/data
WORKDIR /app

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY ["download.py", "docker-entrypoint.sh", "./"]
COPY bir /app/bir
COPY tests /app/tests
