import io
import datetime as dt

import pandas as pd

import bir


def _bacen_text_to_dataframe(text: str) -> pd.DataFrame:
    """
    This function converts the raw data given by brazilian central bank to
    a pandas dataframe.

    Input:
    ---------
    text: str
        Example:
        '14122020;220;A;USD;5,0572;5,0578;1,0000;1,0000\n'
        '15122020;220;A;USD;5,0962;5,0968;1,0000;1,0000\n'
        '16122020;220;A;USD;5,1051;5,1057;1,0000;1,0000\n'

    Returns:
    ---------
    pd.DataFrame
        Example:
                    data ticker  compra   venda
            0 2020-12-14    USD  5.0572  5.0578
            1 2020-12-15    USD  5.0962  5.0968
            2 2020-12-16    USD  5.1051  5.1057

    """

    df = pd.read_csv(
        io.StringIO(text),
        sep=";",
        decimal=",",
        names=[
            "data",
            0,
            1,
            "ticker",
            "compra",
            "venda",
            "usd_par1",
            "usd_par2",
        ],
        parse_dates=["data"],
        date_parser=lambda x: dt.datetime.strptime(str(x), "%d%m%Y"),
    )
    df["data"] = pd.to_datetime(df["data"])
    df.drop(columns=[0, 1, "usd_par1", "usd_par2"], inplace=True)
    return df


def get(start: dt.datetime, end: dt.datetime = None) -> pd.DataFrame:
    """
    Get USD and EUR ptax from BACEN (brazilian central bank) given dates range
    This function uses the mid between bid and ask as PTAX rate
    Also it only returns dates given by bacen, a holiday may be missing
    If a price for one of the currencies is missing also, it will carry the
        value from the last day

    Input:
    ---------
    start: dt.date
        The date in which the prices begin

    end: dt.date = dt.today()
        The date in which the prices ends.
        Must be grater then `start`
        Defaults to today

    Returns:
    ---------
    pd.DataFrame
        Columns:
            data: dt.datetime
            USD: float
            EUR: float
        Example:
                    data    USD     EUR
            0 2020-12-14 5.0572  5.5578
            1 2020-12-15 5.0962  5.5968
            2 2020-12-16 5.1051  5.6057
    """

    if end is None:
        end = dt.datetime.now()
        end = end.replace(hour=0, minute=0, second=0, microsecond=0)

    if (not isinstance(start, (dt.datetime, dt.date))) or (
        not isinstance(end, (dt.datetime, dt.date))
    ):
        raise TypeError('"start" and "end" field should be datetime')

    if end < start:
        msg = f'End date "{end}" must be latter then start date "{start}"'
        raise ValueError(msg)

    text = bir.bacen.download(start, end, "USD")
    df_usd = _bacen_text_to_dataframe(text)
    df_usd["mid"] = (df_usd["compra"] + df_usd["venda"]) / 2
    text = bir.bacen.download(start, end, "EUR")
    df_eur = _bacen_text_to_dataframe(text)
    df_eur["mid"] = (df_eur["compra"] + df_eur["venda"]) / 2

    df = pd.merge(
        left=df_usd[["data", "mid"]],
        right=df_eur[["data", "mid"]],
        on=["data"],
        how="inner",
        suffixes=("_usd", "_eur"),
    )
    df.columns = ["data", "USD", "EUR"]
    df.fillna(method="ffill", inplace=True)
    return df
