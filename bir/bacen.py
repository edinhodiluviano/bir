import datetime as dt

import requests


class HTTPError(Exception):
    """Exception for unexpected HTTP status codes"""

    def __init__(self, response, *args, **kwargs):
        self.response = response
        self.request = response.request
        msg = (
            "Status code not 200:\n"
            f"{self.request.url=}\n"
            f"{self.request.params=}\n"
            f"{self.response.status_code=}\n"
            f"{self.response.text=}\n"
        )
        super().__init__(msg, *args, **kwargs)


def download(start: dt.date, end: dt.date, currency: str) -> str:
    """
    Simple function to download data from one currency from bacen web service

    Input:
    ---------
    start: dt.date
        The date in which the prices begin

    end: dt.date
        The date in which the prices ends.
        Must be grater then `start`

    currency: str
        The currency to be downloaded
        Supported values are "USD" and "EUR" only.

    Returns:
    ---------
    str
        Lines with the data returned by bacen
        Example:
        '14122020;220;A;USD;5,0572;5,0578;1,0000;1,0000\n'
        '15122020;220;A;USD;5,0962;5,0968;1,0000;1,0000\n'
        '16122020;220;A;USD;5,1051;5,1057;1,0000;1,0000\n'
    """

    if end < start:
        msg = f'End date "{end}" must be latter then start date "{start}"'
        raise ValueError(msg)

    if not isinstance(currency, str) or currency not in {"USD", "EUR"}:
        msg = f'Currency "{currency}" not implemented'
        raise ValueError(msg)

    base = "https://ptax.bcb.gov.br/ptax_internet/consultaBoletim.do"

    currency_code = {"USD": 61, "EUR": 222}
    params = dict(
        method="gerarCSVFechamentoMoedaNoPeriodo",
        ChkMoeda=currency_code[currency],
        DATAINI=start.strftime("%d/%m/%Y"),
        DATAFIM=end.strftime("%d/%m/%Y"),
    )

    r = requests.get(base, params=params, timeout=5)
    if r.status_code != 200:
        raise HTTPError(r)

    return r.text
