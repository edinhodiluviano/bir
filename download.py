#!/usr/bin/env python


import argparse
import datetime as dt

import bir


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("start", help="Prices start date", type=str)
    parser.add_argument(
        "-e",
        "--end",
        help="Prices end date, defaults to today",
        type=str,
    )
    parser.add_argument(
        "-f",
        "--filename",
        help='Output file name, defaults to "output.csv"',
        default="output.csv",
        type=str,
    )
    args = parser.parse_args()

    args.start = dt.datetime.fromisoformat(args.start)
    if args.end:
        args.end = dt.datetime.fromisoformat(args.end)

    return args


def _save(df, filename):
    df.to_csv(filename, index=False)


def main():
    args = _parse_args()
    df = bir.get(args.start, args.end)
    _save(df, args.filename)


if __name__ == "__main__":
    main()
