# bir

Belo Investment Research challenge.
https://pyjobs.com.br/job/1906/challenge_submit/


# Original Challenge text

Nosso desafio para você:

Como trabalhamos com dados de investimentos, o seu dia a dia será de criação e manutenção de diversos bots e tasks que rodam em nosso ambiente para nossos modelos econométricos e econômicos.

Um dos dados essenciais para estes tipos de análise é o Dólar e Euro PTAX - a PTAX é a média de todas as transações realizadas em casas de câmbio em determinados horários - eles permitem a aferição do risco de mercado e da valorização de carteiras de nossos clientes.

Nosso desafio é o desenvolvimento de um scraper que, dado 2 datas de intervalo, permite o download e armazenamento dos dados da PTAX de Dólar e Euro em um arquivo CSV (contendo a data; PTAX Dólar da data; PTAX Euro da data).

Para a fonte de dados, o Banco Central do Brasil (BACEN) fornece um boletim com valores históricos: https://www.bcb.gov.br/estabilidadefinanceira/historicocotacoes

O que gostaríamos de ver:


    Python mais recente

    Boa cobertura de testes

    Criação de um ambiente Docker (dockerfile + docker-compose)

Exercício Bônus:

Caso você queira se aprofundar no mundo dos scrapers e ajudar as pessoas envolvidas no CoVid-19, repita o mesmo exercício de scraping para os dados da Bloomberg https://www.bloomberg.com/graphics/2020-united-states-coronavirus-outbreak/?srnd=coronavirus.

Caso você tenha alguma dúvida, mande um e-mail para: vinicius@pyjobs.com.br


# General instructions

### User interface (running the program)
`./download.py 2020-12-01` will retrieve the data from the given date until
today and save on "output.csv" file

`./download.py 2020-12-01 --end 2020-12-15` retrieves the data from Dec-1st
to Dec-15th, 2020 and saves to "output.csv" file

`./download.py 2020-12-01 --filename my-output.csv` retrieves the data
from Dec-1st until today and saves to "my-output.csv" file

`./download.py -h` or `./download.py --help` show all the options


### Instalation

##### On a virtual environment
For using the software:
`pip install requirements.txt`

##### On docker
`docker-compose up`

##### For developing
`poetry install`

### To run the unit tests
`pytest`

### To run the integration tests
`pytest tests/integration`  
As the integration tests may take longer, usually I put them on a
separated test suite.  
Although for this small project the time factor alone wouldn't make much of
a difference, I kept this way for organization and decouple the tests from
the external services (I don't want a test to fail because bacen services are
down).


# General comments

### Why a "bacen" module?
I decided that a dedicated module couldprovide a better abstraction and more
decoupling to the project.  
If some day the connection requisites changes (for instance a cache, or the
need for a proxy) this is the module we need to change.  

### Which price are you using?
Mid. It is the simple average between bid and ask.  
Some fund administrators may use the sell price, but even more common would
be the BMF-Dolar future settle price.  

### What happens to holidays?
It will be shown dates that bacen publishes PTAX for at least one rate.  
If only one rate is not available, the program will repeat its last price.  

### Why are the currency codes hard-coded?
Just for simplicity in a small test. For real I'd rather use a config or
yml file with all currencies and codes.

### Why pandas instead of built in csv module?
Pandas offers a flexible io. It is just easier to use its parsers and writers.  
Also it makes life easier if we want to write a different format in future
(like .parquet).

### What are the test_should_always_pass tests?
They just test imports (and, with that, some of the project structure).
