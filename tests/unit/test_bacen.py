import datetime as dt
from unittest.mock import patch, Mock

import pytest

import bir


@patch("bir.bacen.requests")
def test_download_ok(mock_requests):
    mock_response = Mock()
    mock_response.status_code = 200
    mock_requests.get.return_value = mock_response
    resp = bir.bacen.download(dt.date(2020, 1, 1), dt.date(2020, 1, 1), "USD")
    assert resp is mock_response.text
    args_list = mock_requests.get.call_args_list
    assert len(args_list) == 1
    assert "bcb.gov.br" in args_list[0][0][0]


@patch("bir.bacen.requests")
def test_download_wrong_status_code(mock_requests):
    for status_code in range(1, 600):
        if status_code == 200:
            continue
        mock_response = Mock()
        mock_response.status_code = status_code
        with pytest.raises(bir.bacen.HTTPError):
            _ = bir.bacen.download(
                dt.date(2020, 1, 1), dt.date(2020, 1, 1), "USD"
            )


def test_download_wrong_dates():
    with pytest.raises(ValueError):
        _ = bir.bacen.download(dt.date(2020, 1, 1), dt.date(2019, 1, 1), "USD")


def test_download_wrong_currency():
    for currency in ["BRL", "AUR", "usd", "usd/brl", "JPY", "", None, 1, []]:
        with pytest.raises(ValueError):
            _ = bir.bacen.download(
                dt.date(2020, 1, 1), dt.date(2020, 2, 1), currency
            )
