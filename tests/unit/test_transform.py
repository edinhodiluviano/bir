import itertools
import datetime as dt
from unittest.mock import patch

import pytest
import numpy as np

import bir


@pytest.fixture
def sample_text():
    s = (
        "14122020;220;A;USD;5,0572;5,0578;1,0000;1,0000\n"
        "15122020;220;A;USD;5,0962;5,0968;1,0000;1,0000\n"
        "16122020;220;A;USD;5,1051;5,1057;1,0000;1,0000\n"
    )
    return s


@pytest.fixture
def mock_download(sample_text):
    with patch("bir.bacen.download") as mock_download:
        mock_download.return_value = sample_text
        yield mock_download


def test_should_always_pass():
    return


def test_bacen_text_to_dataframe(sample_text):
    df = bir.transform._bacen_text_to_dataframe(sample_text)
    assert df.shape == (3, 4)
    assert list(df.columns) == ["data", "ticker", "compra", "venda"]
    assert df["ticker"].values[-1] == "USD"
    assert df["data"].values[0] == np.datetime64("2020-12-14")
    assert df["compra"][0] == 5.0572


def test_get_ok(mock_download):
    df = bir.transform.get(dt.datetime(2020, 1, 1), dt.datetime(2021, 1, 1))
    assert df.shape == (3, 3)
    assert list(df.columns) == ["data", "USD", "EUR"]
    assert df.USD[0] == (5.0572 + 5.0578) / 2
    assert df.EUR[0] == (5.0572 + 5.0578) / 2


def test_get_wrong_arg_type():
    args = ["", None, 1, 1.1, "1", "10/01/2020", "2020-10-01", []]
    for arg1, arg2 in itertools.product(args, repeat=2):
        with pytest.raises(TypeError):
            _ = bir.transform.get(arg1, arg2)


def test_get_wrong_data_values():
    with pytest.raises(ValueError):
        _ = bir.transform.get(dt.datetime(2021, 1, 1), dt.datetime(2020, 1, 1))


def test_get_default_end_date(mock_download):
    _ = bir.get(dt.datetime(2020, 1, 1))
    call_list = mock_download.call_args_list
    assert len(call_list) == 2
    call_list[0][0][0] == dt.datetime(2020, 1, 1)
    call_list[0][0][1] == dt.date.today()
