import os
import subprocess
import datetime as dt
from unittest.mock import patch

import pytest
import numpy as np
import pandas as pd

import bir
import download


@pytest.fixture(autouse=True)
def clean_output_files():
    for file in ["output.csv", "2.csv"]:
        try:
            os.remove(file)
        except FileNotFoundError:
            pass


@pytest.fixture
def sample_text():
    s = (
        "14122020;220;A;USD;5,0572;5,0578;1,0000;1,0000\n"
        "15122020;220;A;USD;5,0962;5,0968;1,0000;1,0000\n"
        "16122020;220;A;USD;5,1051;5,1057;1,0000;1,0000\n"
    )
    return s


@pytest.fixture
def expected_df():
    df = pd.DataFrame(
        {
            "data": [
                dt.datetime(2020, 12, 14),
                dt.datetime(2020, 12, 15),
                dt.datetime(2020, 12, 16),
            ],
            "USD": [5.0575, 5.0965, 5.1054],
            "EUR": [5.0575, 5.0965, 5.1054],
        }
    )
    return df


@pytest.fixture
def mock_download(sample_text):
    with patch("bir.bacen.download") as mock_download:
        mock_download.return_value = sample_text
        yield mock_download


def check_dfs(df1, df2):
    assert np.array_equal(df1.columns, df2.columns)
    assert np.array_equal(df1.dtypes, df2.dtypes)
    assert np.array_equal(df1["data"], df2["data"])
    # prevent floating point issues
    df1 = df1.round(decimals=4)
    df2 = df2.round(decimals=4)
    assert df1.equals(df2)


def test_from_get(mock_download, expected_df):
    df = bir.get(dt.datetime(2020, 1, 1), dt.datetime(2021, 1, 1))
    check_dfs(df, expected_df)


def run_bash(command: str):
    process = subprocess.run(command.split(), capture_output=True)
    output = process.stdout.decode("utf8")
    error = process.stderr.decode("utf8")
    return output, error


def test_download_script(mock_download, expected_df):
    with patch("download.argparse._sys.argv", ["", "2020-03-15"]):
        download.main()
    call_list = mock_download.call_args_list
    assert len(call_list) == 2
    assert call_list[0][0][0] == dt.datetime(2020, 3, 15)
    now = dt.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    assert call_list[0][0][1] == now
    assert os.path.exists("output.csv")
    df = pd.read_csv("output.csv", parse_dates=["data"])
    check_dfs(df, expected_df)
    os.remove("output.csv")


def test_e2e(expected_df):
    output, error = run_bash("./download.py 2020-12-14 -e 2020-12-16")
    assert error == ""
    assert os.path.exists("output.csv")
    df = pd.read_csv("output.csv", parse_dates=["data"])
    # my simple mock only returns the same values
    expected_df["EUR"] = df["EUR"]
    check_dfs(df, expected_df)
    os.remove("output.csv")


def test_e2e_with_specific_filename(mock_download, expected_df):
    output, error = run_bash("./download.py 2020-12-14 -e 2020-12-16 -f 2.csv")
    assert error == ""
    assert os.path.exists("2.csv")
    df = pd.read_csv("2.csv", parse_dates=["data"])
    # my simple mock only returns the same values
    expected_df["EUR"] = df["EUR"]
    check_dfs(df, expected_df)
    os.remove("2.csv")


def test_e2e_with_incorrect_arg_option():
    for args in [
        "./download.py",
        "./download.py 2020-12-14 2020-12-16",
        "./download.py -s 2020-12-14 -e 2020-12-16",
        "./download.py 2020-12-14 -v",
        "./download.py 2020-12-14 -x casklmcds",
    ]:
        output, error = run_bash(args)
        assert error != ""
        assert not os.path.exists("output.csv")


def test_e2e_with_incorrect_arg_value_type():
    for args in [
        "./download.py caskjcsd",
        "./download.py 2020-12-14 -e aldksmcasd",
        "./download.py 2020-12-14 -e 20201216",
        "./download.py 2020-12-14 -e 2020/12/16",
        "./download.py 2020-12-14 -e 16/12/2020",
        "./download.py 2020-12-14 -f",
    ]:
        output, error = run_bash(args)
        assert error != ""
        assert not os.path.exists("output.csv")
